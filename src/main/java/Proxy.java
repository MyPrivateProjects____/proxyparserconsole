import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import com.maxmind.geoip.Location;
import com.maxmind.geoip.LookupService;


public class Proxy {
    public static final Pattern IP_PORT_PATTERN = Pattern.compile("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]):[0-9]+$");
    public static final Pattern IP_PATTERN = Pattern.compile("((1?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}(1?\\d\\d?|2[0-4]\\d|2\u200C\u200B5[0-5])+");


    public static Set<Proxy> PROXY = new HashSet<>();
    public static Set<Proxy> PROXYGOOD = new HashSet<>();
    public static Set<Proxy> PROXYBAD = new HashSet<>();
    public static Set<Proxy> PROXYVERIFIED = new HashSet<>();


    private String ip;
    private String port;
    private long delay;
    private InetSocketAddress address;
    private boolean isAddressReachable;

    private String geodata = getClass().getResource("/GeoIP.dat").getFile();
    private String geocity = getClass().getResource("/GeoLiteCity.dat").getFile();


    Proxy(String ip, String port) throws UnknownHostException {
        this.ip = ip;
        this.port = port;
        address = new InetSocketAddress(ip, Integer.parseInt(port));

    }


    public void setCountryCity (Proxy proxy){
        try {
            geodata = getCountry();
            geocity = getCity();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public Proxy(String ipPort) {

        if (ipPort.contains(":")) {
            this.ip = ipPort.substring(0, ipPort.indexOf(":"));
            this.port = ipPort.substring(ipPort.indexOf(":") + 1, ipPort.length());
            if (!(Integer.parseInt(port)< 0 || Integer.parseInt(port) > 0xFFFF)) {
                address = new InetSocketAddress(ip, Integer.parseInt(port));
            }else {
                //Throw NotProxyException
            }
            System.out.println(this.toString());
        } else {
            //Throw NotProxyException
        }

    }

    public boolean isAddressReachable() {
        return isAddressReachable;
    }

    public void setAddressReachable(boolean addressReachable) {
        isAddressReachable = addressReachable;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public void setDelay(long delay) {
        this.delay = delay;
    }

    public InetSocketAddress getInetSocketAdress() {
        return address;
    }

    public void setInetSocketAddress(InetSocketAddress address) {
        this.address = address;
    }

    public String getGeodata() {
        return geodata;
    }

    public void setGeodata(String geodata) {
        this.geodata = geodata;
    }

    public String getGeocity() {
        return geocity;
    }

    public void setGeocity(String geocity) {
        this.geocity = geocity;
    }

    public String getAnonymity() {
        return anonymity;
    }

    public void setAnonymity(String anonymity) {
        this.anonymity = anonymity;
    }

    private String anonymity;


    public String getIp() {
        return this.ip;
    }

    public String getPort() {
        return this.port;
    }


    public double getDelay() {
        return (double) delay / 1000.0;
    }

    public String getHostName() {
        return address.getHostName();

    }


    public String getCountry() throws IOException {
        LookupService cl = new LookupService(geodata, LookupService.GEOIP_MEMORY_CACHE);
        String countrycode = cl.getCountry(ip).getCode();
        System.out.println(countrycode);
        cl.close();

        return countrycode;
    }

    public String getCity() throws IOException {
        LookupService cl = new LookupService(geocity, LookupService.GEOIP_MEMORY_CACHE);
        System.out.println(ip);
        Location l1 = cl.getLocation(ip);

        String city;
        if (l1 != null) {
            city = l1.city;
        } else {
            city = "--";
        }
        cl.close();
        return city;

    }


    public String toStringIpPort() {
        return getIp() + ":" + getPort();
    }

    public String toStringIpPortChecked() {
        return getIp() + ":" + getPort() + "," + getDelay();
    }


    @Override
    public String toString() {
        return "Proxy{" +
                "ip='" + ip + '\'' +
                ", port='" + port + '\'' +
                ", delay=" + delay +
                ", address=" + address +
                ", geodata='" + geodata + '\'' +
                ", geocity='" + geocity + '\'' +
                ", anonymity='" + anonymity + '\'' +
                ", timeout=" + /*timeout */+
                '}';
    }
/* public String getAnonLevel() throws IOException {

        ArrayList<String> stuff = new WebUrl(anonChecker,ip,port).getData();
        if (stuff == null) return "Time Out";

        return stuff.get(0);
    }*/


}
