import java.io.*;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

enum FileType {
    GOODPROXY("goodproxy.txt"),
    BADPROXY("badproxy.txt"),
    PROXYALL("proxy.txt"),
    URLS("urls.txt");

    FileType(String s) {
        this.s = s;
    }

    public String getS() {
        return s;
    }

    private String s;
}

public class FIleRW {

    Logger log = LoggerFactory.getLogger(FIleRW.class);


    void FileLineWriter(String line, FileType ft) {
        File file = new File(ft.getS());

        log.info("Created FileObject for writing: " + ft.getS());

        try (FileWriter fileWriter = new FileWriter(file, true);
             BufferedWriter bufferWriter = new BufferedWriter(fileWriter)) {

            switch (ft) {
                case BADPROXY:
                    if (!Proxy.PROXYBAD.contains(line)) {
                        log.info("PROXYBAD not contain this line: " + line);

                        bufferWriter.write(line + "\n");
                        bufferWriter.close();
                        Proxy.PROXY.add(new Proxy(line));

                        log.info("add to PROXYBAD this: " + line);
                    }
                    break;
                case GOODPROXY:
                    if (!Proxy.PROXYGOOD.contains(line)) {
                        log.info("PROXYGOOD not contain this line: " + line);

                        bufferWriter.write(line + "\n");
                        bufferWriter.close();
                        Proxy.PROXYGOOD.add(new Proxy(line));

                        log.info("add to PROXYGOOD this: " + line);
                    }
                    break;
                case PROXYALL:
                    if (!Proxy.PROXY.contains(line)) {
                        log.info("PROXY not contain this line: " + line);

                        bufferWriter.write(line + "\n");
                        bufferWriter.close();
                        Proxy.PROXY.add(new Proxy(line));

                        log.info("add to PROXY this: " + line);
                    }
                    break;
                case URLS:
                    if (!WebUrl.URLS.contains(line)) {
                        log.info("URLS not contain this line: " + line);

                        bufferWriter.write(line + "\n");
                        bufferWriter.close();
                        WebUrl.URLS.add(line);

                        log.info("add to URLS this: " + line);
                    }
                default:
                    break;
            }
        } catch (FileNotFoundException e) {
            log.error("File not found to write: " + ft.getS());
        } catch (IOException e) {
            log.error("IOException when write in: " + ft.getS());
        }
    }

    void FileLineReader(FileType ft) {
        File file = new File(ft.getS());
        log.info("Created FileObject for reading: " + ft.getS());
        try (FileReader fileReader = new FileReader(file);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String s;
            switch (ft) {
                case BADPROXY:
                    while ((s = bufferedReader.readLine()) != null) {
                        Proxy.PROXYBAD.add(new Proxy(s));
                        log.info("Read file: " + ft.getS() + "add to PROXYBAD this: " + s);
                    }
                    break;
                case GOODPROXY:
                    while ((s = bufferedReader.readLine()) != null) {
                        Proxy.PROXYGOOD.add(new Proxy(s));
                        log.info("Read file: " + ft.getS() + "add to PROXYGOOD this: " + s);
                    }
                    break;
                case PROXYALL:
                    while ((s = bufferedReader.readLine()) != null) {
                        Proxy.PROXY.add(new Proxy(s));
                        log.info("Read file: " + ft.getS() + "add to PROXY this: " + s);
                    }
                    break;
                case URLS:
                    while ((s = bufferedReader.readLine()) != null) {
                        WebUrl.URLS.add(s);
                        log.info("Read file: " + ft.getS() + "add to URL this: " + s);
                    }
                default:
                    break;
            }

        } catch (FileNotFoundException e) {
            log.error("File not found to read: " + ft.getS());
        } catch (IOException e) {
            log.error("IOException when read in: " + ft.getS());
        }
    }

}
