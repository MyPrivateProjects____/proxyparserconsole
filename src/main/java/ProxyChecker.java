


import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;

import java.io.IOException;
import java.net.InetAddress;
import java.util.regex.Matcher;

public class ProxyChecker implements Runnable{

    private static int timeout = 3000;

    //CheckProxy for valid ip and port

    public static String IpPortMatcher(String s) {

        Matcher m = Proxy.IP_PORT_PATTERN.matcher(s);

        while (m.find()) {
            int start = m.start(0);
            int end = m.end(0);

            System.out.println(s.substring(start, end));
            return s.substring(start, end);
        }
        return null;
    }

    //CheckProxy ping

    public boolean isAlive(Proxy proxy) throws IOException {
        boolean status = false;
        long t0 = System.currentTimeMillis();
        if (proxy.getInetSocketAdress().getAddress().isReachable(timeout)) {
            status = true;
        }
        long t1 = System.currentTimeMillis();
        proxy.setDelay(t1 - t0);
        return status;
    }


    //CheckProxy for alive

    public void checkproxies() {
        try {
            for (Proxy proxy : Proxy.PROXY) {
                System.out.println(proxy.toStringIpPort()); //log
                String list = proxy.toStringIpPort();
                String[] hp = list.split(":");
                InetAddress addr = null;
                if (!(Integer.parseInt(hp[1])< 0 || Integer.parseInt(hp[1]) > 0xFFFF)) {
                    addr = InetAddress.getByName(hp[0]);
                }else {
                    //notproxy
                }
                if (addr.isReachable(5000)) {
                    System.out.println("reached");
                    ensocketize(hp[0], Integer.parseInt(hp[1]));
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    //CheckProxy for reaching url

    public void ensocketize(String host, int port) {
        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet get = new HttpGet("https://facebook.com/");
            HttpHost proxy = new HttpHost(host, port);
            client.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
            client.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 10000);
            HttpResponse response = client.execute(get);

            if (response != null) {
                System.out.println(response.getStatusLine());
                System.out.println(response.toString());
                System.out.println(host + ":" + port + " @@ working");




                new FIleRW().FileLineWriter(host + ":" + port, FileType.GOODPROXY);
            }
        } catch (Exception ex) {
            System.out.println("Proxy failed");
        }
    }

    @Override
    public void run() {
        while(true) {
            int size = 0;
            if (Proxy.PROXY.size() != size) {
                System.out.println("провірка");
                ProxyChecker proxyChecker = new ProxyChecker();
                proxyChecker.checkproxies();
                size = Proxy.PROXY.size();
                System.out.println(size);
            } else {
                System.out.println("передача потоку");

                Thread.yield();
            }


        }
    }

//    @Test
//    public void testProxySystem() throws Exception {
//
//        Proxy used = GhostMe.ghostMySystemProperties(true);
//
//        ProxyBinResponse response = GhostMeHelper.getMyInformation();
//        assertEquals(used.getIp().trim(), response.getOrigin().trim());
//        assertEquals(used.getIp().trim(), response.getHeaders().get("X-Forwarded-For"));
//        assertEquals(used.getIp().trim(), response.getHeaders().get("X-Real-Ip"));
//    }


    /*@Test
    public void testProxyConnection() throws Exception {

        Proxy used = GhostMe.getProxy(true);

        ProxyBinResponse response = GhostMeHelper.getMyInformation(used.getJavaNetProxy());
        assertEquals(used.getIp().trim(), response.getOrigin().trim());
        assertEquals(used.getIp().trim(), response.getHeaders().get("X-Forwarded-For"));
        assertEquals(used.getIp().trim(), response.getHeaders().get("X-Real-Ip"));
    }*/


    /**
     * Test function to get random user agents
     */
/*
    @Test
    public void testUserAgent() {
        String agent = GhostMe.getRandomUserAgent();
        System.out.println("Agent is: " + agent);

        assertNotNull(agent);
        assertTrue(agent.contains("Mozilla/5.0"));
    }
*/

}