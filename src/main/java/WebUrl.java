import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.*;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.SocketAddress;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class WebUrl{

    public static Set<String> URLS = new HashSet<>();


    public static void main(String[] args) throws InterruptedException {
        System.out.println(System.getProperties().put("180.148.33.29:80", "true"));
       // Document html = Jsoup.parse(new WebUrl("http://php.spb.ru/proxy/","180.148.33.29", "80" ).parseWithScripts());
     //   System.out.println(html.body().getElementsByAttributeValue("class","col-sm-9 font-weight-bold").text());
    }

    private String url,ip,port;
    private boolean useProxy;

    WebUrl(String url){
        this.url = url;

    }

    WebUrl(String url, String ip, String port){
        this.url = url;
        this.ip = ip;
        this.port = port;
        useProxy = true;
    }


    public boolean waitForJSandJQueryToLoad(WebDriver driver) {

        WebDriverWait wait = new WebDriverWait(driver, 30);

        // wait for jQuery to load
        ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    return ((Long)((JavascriptExecutor)driver).executeScript("return jQuery.active") == 0);
                }
                catch (Exception e) {
                    // no jQuery present
                    return true;
                }
            }
        };

        // wait for Javascript to load
        ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor)driver).executeScript("return document.readyState")
                        .toString().equals("complete");
            }
        };

        return wait.until(jQueryLoad) && wait.until(jsLoad);
    }

    private String parseWithScripts() throws InterruptedException {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addExtensions(new File("Block-image_v1.0.crx"));
        chromeOptions.addArguments("--proxy-server=" + ip + ":" + port);

        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);


        WebDriver driver = new ChromeDriver(capabilities);

        driver.get(url);

        while (!waitForJSandJQueryToLoad(driver)){
            Thread.sleep(1000);
        }

        //КОСТИЛЬ
        Thread.sleep(500000);


        String pageSource = driver.getPageSource();

        driver.close();
        driver.quit();
        return pageSource;
    }


    public ArrayList<String> getData() {

        ArrayList<String> data = new ArrayList<String>();
        String line=null;
        URL address = null;
        try {
            address = new URL(url);
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        URLConnection urlconn = null;



        if (useProxy) {
            SocketAddress addr = new InetSocketAddress(ip, Integer.parseInt(port));
            java.net.Proxy httpProxy = new java.net.Proxy(java.net.Proxy.Type.HTTP, addr);
            try {
                urlconn = address.openConnection(httpProxy);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }else {
            try {
                urlconn = address.openConnection();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        try {
            urlconn.connect();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            return null;
        }
        BufferedReader buffreader = null;
        try {
            buffreader = new BufferedReader(new InputStreamReader(urlconn.getInputStream()));
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        try {
            line = buffreader.readLine();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        while (line!=null) {

            data.add(line);
            System.out.println(line);

            try {
                line = buffreader.readLine();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return data;
    }
}