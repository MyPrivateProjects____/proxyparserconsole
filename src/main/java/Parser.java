import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.*;
import java.util.regex.Matcher;

import java.io.*;

public class Parser implements Runnable {

    public static final String DIGITREGEX = "\\d+";
    public static final String REPLASEDCHARS = "\\<.*?>|<!--|-->|:";
    public static final String SPLITREGEX = "'|\"| |\n|\t";

    // WAIT selenium for loading scripts
    public static boolean waitForJSandJQueryToLoad(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        // wait for jQuery to load
        ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    return ((Long) ((JavascriptExecutor) driver).executeScript("return jQuery.active") == 0);
                } catch (Exception e) {
                    // no jQuery present
                    return true;
                }
            }
        };

        // wait for Javascript to load
        ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor) driver).executeScript("return document.readyState")
                        .toString().equals("complete");
            }
        };

        return wait.until(jQueryLoad) && wait.until(jsLoad);
    }

    //connect to site for parsing

    public static String connect(String url) throws InterruptedException {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addExtensions(new File("Block-image_v1.0.crx"));

        WebDriver driver = new ChromeDriver(chromeOptions);
        driver.get(url);

        while (!waitForJSandJQueryToLoad(driver)) {
            Thread.sleep(1000);
        }

        //КОСТИЛЬ
        Thread.sleep(5000);


        String pageSource = driver.getPageSource();

        //click on next pages
        for (int i = 2; i < 1000; i++) {
            try {
                driver.findElement(By.linkText(String.valueOf(i))).click();
                System.out.println(By.linkText(String.valueOf(i)));

                while (!waitForJSandJQueryToLoad(driver)) {
                    Thread.sleep(1000);
                }
                //КОСТИЛЬ
                Thread.sleep(3000);
                pageSource += driver.getPageSource();

            } catch (NoSuchElementException e) {
                break;
            } finally {
                System.out.println("not found");
            }
        }
        driver.close();
        driver.quit();
        return pageSource;
    }

    void parse() {
        for (String s : WebUrl.URLS) {
            System.out.println(s);

            String source = null;
            try {
                source = connect(s);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            source = source.replaceAll(REPLASEDCHARS, " ");

            String[] result = source.split(SPLITREGEX);
            ArrayList<String> results = new ArrayList<>(Arrays.asList(result));


            for (int i = 0; i < results.size(); i++) {
                if (results.get(i).equals("") || results.get(i).matches(".*[a-zA-Z]+.*") || results.get(i).contains(":")) {
                    results.remove(i);
                    i--;
                }
            }

            findProxyes(results);

        }
    }

    void findProxyes(ArrayList<String> results){
        for (int i = 0; i < results.size() - 1; i++) {
            {

                Matcher m1 = Proxy.IP_PATTERN.matcher(results.get(i));

                while (m1.find()) {
                    if (results.get(i + 1).matches(DIGITREGEX)) {
                        results.set(i, results.get(i) + ":" + results.get(i + 1));
                        if (ProxyChecker.IpPortMatcher(results.get(i)) != null)

                            //
                            new FIleRW().FileLineWriter(results.get(i), FileType.PROXYALL);

                    }
                }
            }
        }
    }



    @Override
    public void run() {
        new FIleRW().FileLineReader(FileType.URLS);
        parse();

    }
}
